# alpha = reciprocal links / all links
# beta = symmetric / all links
# update alpha, beta

SELECT sender,
count(*) AS totLink1,
sum(case when edge_type > 0 then 1 else 0 end) AS recLink1,
sum(case when edge_type = 1 then 1 else 0 end) AS symLink1
INTO iheart1
FROM iheart_edge_info
GROUP BY sender;

SELECT receiver,
count(*) AS totLink2,
sum(case when edge_type > 0 then 1 else 0 end) AS recLink2,
sum(case when edge_type = 1 then 1 else 0 end) AS symLink2
INTO iheart2
FROM iheart_edge_info
GROUP BY receiver;

SELECT iheart1.*, iheart2.*
INTO iheart_link
FROM iheart1
FULL OUTER JOIN iheart2
ON iheart1.sender = iheart2.receiver;

UPDATE iheart_link
SET sender = receiver
WHERE sender IS NULL;

UPDATE iheart_link
SET 
totLink1 = case when totLink1 IS NULL then 0 else totLink1 end,
recLink1 = case when recLink1 IS NULL then 0 else recLink1 end,
symLink1 = case when symLink1 IS NULL then 0 else symLink1 end,
totLink2 = case when totLink2 IS NULL then 0 else totLink2 end,
recLink2 = case when recLink2 IS NULL then 0 else recLink2 end,
symLink2 = case when symLink2 IS NULL then 0 else symLink2 end
WHERE 
totLink1 IS NULL
OR recLink1 IS NULL 
OR symLink1 IS NULL
OR totLink2 IS NULL
OR recLink2 IS NULL
OR symLink2 IS NUll;

ALTER TABLE iheart_link
RENAME sender to node;

ALTER TABLE iheart_link
ADD COLUMN recLink INT,
ADD COLUMN symLink INT,
ADD COLUMN totLink INT;

UPDATE iheart_link
SET
recLink = recLink1 + recLink2,
symLink = symLink1 + symLink2,
totLink = totLink1 + totLink2;

ALTER TABLE iheart_link
DROP COLUMN totLink1,
DROP COLUMN recLink1,
DROP COLUMN symLink1,
DROP COLUMN totLink2,
DROP COLUMN recLink2,
DROP COLUMN symLink2,
DROP COLUMN receiver;

DROP TABLE iheart1;
DROP TABLE iheart2;

ALTER TABLE iheart_link
ADD COLUMN alpha DECIMAL(8,3),
ADD COLUMN beta DECIMAL(8,3);

UPDATE iheart_link
SET alpha = 1.0 * recLink / totLink,
beta = 1.0 * symLink / totLink;

SELECT iheart_nodes.*, iheart_link.alpha, iheart_link.beta
INTO iheart_node
FROM iheart_nodes
LEFT JOIN iheart_link
ON iheart_nodes.node = iheart_link.node;

DROP TABLE iheart_nodes;

SELECT * INTO iheart_nodes FROM iheart_node;

DROP TABLE iheart_node;
