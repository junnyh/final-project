# work log Feb 27
# Create raw data table
CREATE TABLE ismile_raw
( sender INT,
receiver INT,
sending_time INT,
gift_type INT);
\COPY ismile_raw FROM '~/peak_week/ismile2week.txt' DELIMITER ' ';
-------------------------------------------------------------------
# Create nodes tables
SELECT DISTINCT sender INTO ismile_nodes FROM ismile_raw;
SELECT DISTINCT receiver INTO ismile_tmp FROM ismile_raw;

SELECT ismile_nodes.*, ismile_tmp.*
INTO ismile_node
FROM ismile_nodes
FULL OUTER JOIN ismile_tmp
ON ismile_nodes.sender = ismile_tmp.receiver;

DROP TABLE ismile_nodes;
DROP TABLE ismile_tmp;

UPDATE ismile_node
SET sender = receiver
WHERE sender IS NULL;

ALTER TABLE ismile_node
RENAME sender TO node;

ALTER TABLE ismile_node
DROP COLUMN receiver;
-----------------------------------------------------------------------
# node features
# gift sent
SELECT sender, count(sender) AS gift_send
INTO ismile_tmp
FROM ismile_raw
GROUP BY sender;

SELECT ismile_node.*, ismile_tmp.gift_send
INTO ismile_nodes
FROM ismile_node
LEFT JOIN ismile_tmp
ON ismile_node.node = ismile_tmp.sender;

DROP TABLE ismile_tmp;
DROP TABLE ismile_node;

# gift received
SELECT receiver, count(receiver) AS gift_receive
INTO ismile_tmp
FROM ismile_raw
GROUP BY receiver;

SELECT ismile_nodes.*, ismile_tmp.gift_receive
INTO ismile_node
FROM ismile_nodes
LEFT JOIN ismile_tmp
ON ismile_nodes.node = ismile_tmp.receiver;

DROP TABLE ismile_tmp;
DROP TABLE ismile_nodes;

UPDATE ismile_node
SET gift_send = 0
WHERE gift_send IS NULL;

UPDATE ismile_node
SET gift_receive = 0
WHERE gift_receive IS NULL;

# out degree
SELECT sender, count(DISTINCT receiver) AS out_degree
INTO ismile_tmp
FROM ismile_raw
GROUP BY sender;

SELECT ismile_node.*, ismile_tmp.out_degree
INTO ismile_nodes
FROM ismile_node
LEFT JOIN ismile_tmp
ON ismile_node.node = ismile_tmp.sender;

DROP TABLE ismile_tmp;
DROP TABLE ismile_node;

# in degree
SELECT receiver, count(DISTINCT sender) AS in_degree
INTO ismile_tmp
FROM ismile_raw
GROUP BY receiver;

SELECT ismile_nodes.*, ismile_tmp.in_degree
INTO ismile_node
FROM ismile_nodes
LEFT JOIN ismile_tmp
ON ismile_nodes.node = ismile_tmp.receiver;

DROP TABLE ismile_tmp;
DROP TABLE ismile_nodes;

UPDATE ismile_node
SET out_degree = 0
WHERE out_degree IS NULL;

UPDATE ismile_node
SET in_degree = 0
WHERE in_degree IS NULL;









