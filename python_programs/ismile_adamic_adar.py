import os,string

adjacency_list = {}
f_origin = '/home/zejunh/files/peakWeek/ismile_edge_list.txt'

with open(f_origin,'r') as fIn:
	next(fIn)
	for line in fIn:
		origin_line = line.split(',')
		sender = origin_line[0]
		receiver = origin_line[1].strip('\n')
                if sender in adjacency_list:
			if receiver not in adjacency_list[sender]:
                		adjacency_list[sender].append(receiver)
		else:
                	adjacency_list[sender] = [receiver]
		if receiver in adjacency_list:
			if sender not in adjacency_list[receiver]:
                		adjacency_list[receiver].append(sender)
		else:
                	adjacency_list[receiver] = [sender] 

fIn.close()

f_out = '/home/zejunh/files/peakWeek/ismile_adamic_adar.txt'

with open(f_origin, 'r') as fIn, open(f_out, 'a') as fOut:
	next(fIn)
	for line in fIn:
		origin_line = line.split(',')
		sender = origin_line[0]
		receiver = origin_line[1].strip('\n')

		sender_neighbor = adjacency_list[sender]
		receiver_neighbor = adjacency_list[receiver]

		common = 0
		adar = 0
		if len(sender_neighbor) < len(receiver_neighbor):
			for item in sender_neighbor:
				if item in receiver_neighbor:
					common = common + 1
					adar = adar + 1.0 / len(adjacency_list[item])	
		else:
			for item in receiver_neighbor:
				if item in sender_neighbor:
					common = common + 1
					adar = adar + 1.0 / len(adjacency_list[item])
		union = len(sender_neighbor) + len(receiver_neighbor) - 2 - common
		if union > 0:
			jaccard_coef = 1.0 * common/union
		else:
			jaccard_coef = 1
		out_line = str(sender) + ',' + str(receiver) + ',' + str(common) + ',' +  str(jaccard_coef) + ',' + str(adar) + '\n'
		fOut.write(out_line)

fIn.close()
fOut.close()
