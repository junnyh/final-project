import pandas as pd
import numpy as np
from sklearn.svm import SVC
from sklearn.cross_validation import KFold

score = []
df1 = pd.read_csv('/home/zejunh/files/peakWeek/ismile_normal.csv')
#print(df1.info())
data = df1.as_matrix()
#print(data[0,:])
X = data[:,1:]
Y = data[:,0]

n = int(Y.size / 20)
kf = KFold(n, n_folds = 10, shuffle=True)
for train, test in kf:
	#print("%s %s" % (train, test))
	X_train, X_test, Y_train, Y_test = X[train], X[test], Y[train], Y[test]
	clf = SVC(kernel='rbf', C=1.0)
	clf.fit(X_train, Y_train)
	score.append(clf.score(X_test,Y_test))

print score
