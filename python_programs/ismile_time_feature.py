import os,string

time_list = {}
f_origin = '/home/zejunh/files/peakWeek/ismile_time.txt'
          
with open(f_origin,'r') as fIn:
        for line in fIn:
                origin_line = line.split(',')
                sender = int(origin_line[0])
                time = int(origin_line[1].strip('\n'))
                if sender in time_list:
                	time_list[sender].append(time)
		else:
                        time_list[sender] = [time]	
fIn.close()
f_out ='/home/zejunh/files/peakWeek/ismile_send_time_feature.txt'

with open(f_out, 'a') as fOut:
	for item in time_list:
		elapse_time = max(time_list[item]) - min(time_list[item])
		appear_time = len(time_list[item])
		if appear_time > 1:
			time_interval = [x - time_list[item][i-1] for i, x in enumerate(time_list[item])][1:]
			avg_interval = sum(time_interval) / float(len(time_interval))
		else:
			avg_interval = -1
		new_line = str(item) + ',' + str(elapse_time) + ',' + str(appear_time) + ',' + str(avg_interval) + '\n'
		fOut.write(new_line)

fOut.close()	
		
		
