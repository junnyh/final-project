import pandas as pd
import numpy as np
from sklearn import svm
#from sklearn.svm import OneClassSVM
from sklearn.cross_validation import KFold

score = []
df1 = pd.read_csv('/home/zejunh/files/peakWeek/ismile_reciprocal.csv')
df2 = pd.read_csv('/home/zejunh/files/peakWeek/ismile_parasocial.csv')
#print(df1.info())
data1 = df1.as_matrix()
data2 = df2.as_matrix()
#print(data[0,:])
X_positive = data1[:,1:]
X_negative = data2[:,1:]
Y = data1[:,0]
n = int(Y.size / 10)

score1 = []
score2 = []
score3 = []
kf = KFold(n, n_folds = 10, shuffle=True)
for train, test in kf:
	#print("%s %s" % (train, test))
	X_train = X_positive[train]
	X_test = X_positive[test]
	X_outlier = X_negative
	clf = svm.OneClassSVM(nu = 0.1, kernel = "rbf", gamma = 0.1)
	clf.fit(X_train)
	y_pred_train = clf.predict(X_train)
	y_pred_test = clf.predict(X_test)
	y_pred_outliers = clf.predict(X_outlier)
	precision_train = 1.0 * y_pred_train[y_pred_train == 1].size/y_pred_train.size
	precision_test = 1.0 * y_pred_test[y_pred_test == 1].size / y_pred_test.size
	precision_outliers =1.0 * y_pred_outliers[y_pred_outliers == -1].size / y_pred_outliers.size
	score1.append(precision_train)
	score2.append(precision_test)
	score3.append(precision_outliers)
print ("train Score")
print score1
print ("test Score")
print score2
print "outlier Score"
print score3
